package main

import (
	"flag"
	"log"
	"net/http"
)

var (
	appAddr    = flag.String("addr", ":3000", "The addr of the application")
	publicPath = flag.String("publicPath", "public", "Public directory path")
	maxImgW    = flag.Uint("maxImgW", 666, "Max image width")
	maxImgH    = flag.Uint("maxImgH", 666, "Max image height")
	frontUrl   = flag.String("frontUrl", "http://speakcards.tk", "Fronted URL")
)

func main() {
	flag.Parse()

	http.HandleFunc("/add", PostcardAdder)

	log.Print("Serving " + *appAddr)
	log.Fatal(http.ListenAndServe(*appAddr, nil))
}
