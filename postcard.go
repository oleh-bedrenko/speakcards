package main

import (
	"log"
	"net/http"
	"path"
	//"strings"

	"gopkg.in/mgo.v2/bson"
)

func PostcardAdder(w http.ResponseWriter, r *http.Request) {
	text := r.FormValue("text")
	if text == "" {
		w.WriteHeader(http.StatusNoContent)
		return
	}

	file, _, err := r.FormFile("img")
	if err != nil {
		log.Print(err)
		w.WriteHeader(http.StatusNoContent)
		return
	}

	id := bson.NewObjectId()

	if err := MakeTTS(text, false, path.Join(*publicPath, "audios", id.Hex())); err != nil {
		log.Print(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if err = UploadPhoto(file, path.Join(*publicPath, "images", id.Hex())); err != nil {
		log.Print(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	//@todo mongodb

	http.Redirect(w, r, *frontUrl+"#"+id.Hex(), http.StatusTemporaryRedirect)
}
