package main

import (
	"os"
	"regexp"
	"strings"

	ivona "github.com/goleh/tts"
)

func MakeTTS(text string, isFemale bool, filePath string) error {
	output, err := os.Create(filePath)
	if err != nil {
		return err
	}
	defer output.Close()

	lang := ivona.LANG_EN
	if cyrillic, _ := regexp.MatchString("[а-яА-Я]+", text); cyrillic {
		lang = ivona.LANG_RU
	}

	gender := ivona.GENDER_MALE
	if isFemale {
		gender = ivona.GENDER_FEMALE
	}

	if ivona.NewVoice(lang, gender).Say(strings.NewReader(text), output); err != nil {
		os.Remove(filePath)
		return err
	}
	return nil
}
