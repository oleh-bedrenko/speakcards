var apiUrl = "http://speakcards.tk:3000";
var app    = angular.module('app', ['ngRoute']);

app.config(function($routeProvider) {
	$routeProvider.when('/add', {
		templateUrl : 'pages/add.html',
		controller  : 'addController'
	}).when('/:id', {
		templateUrl : 'pages/view.html',
		controller  : 'viewController'
	}).otherwise({
		templateUrl : 'pages/index.html',
		controller  : 'indexController'
	});
});

app.controller('addController', function($scope, $sce) {
	$scope.actionUrl = $sce.trustAsResourceUrl(apiUrl + "/add");
});

app.controller('viewController', function($scope, $sce, $routeParams) {
	$scope.imageUrl = $sce.trustAsResourceUrl("/images/" + $routeParams.id);
	$scope.audioUrl = $sce.trustAsResourceUrl("/audios/" + $routeParams.id);
	$scope.shareUrl = encodeURIComponent(window.location.href);
});

app.controller('indexController', function($scope) {
	$scope.message = 'Latest postcards gonna be there... This part is under constraction!';
});
