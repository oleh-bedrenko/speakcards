package main

import (
	"image"
	_ "image/gif"
	"image/jpeg"
	_ "image/png"
	"io"
	"os"

	"github.com/nfnt/resize"
)

func UploadPhoto(file io.ReadCloser, filePath string) error {
	img, _, err := image.Decode(file)
	if err != nil {
		return err
	}
	file.Close()

	out, err := os.Create(filePath)
	if err != nil {
		return err
	}
	defer out.Close()

	m := resize.Thumbnail(*maxImgW, *maxImgH, img, resize.Lanczos3)

	return jpeg.Encode(out, m, nil)
}
